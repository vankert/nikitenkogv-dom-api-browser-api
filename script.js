const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
            //all needed data is listed below as an entity 
            let created = character.created;
            let species = character.species;
            let img = character.image;
            let episodes = '';
            let name = character.name;
            let location = character.location.name;


            for (let i = 0; i < character.episode.length; i++) {
                episodes = episodes + `<span>${character.episode[i]}</span>`;
            }


            //create element
            const cardsBox = document.querySelector('.cards');


            const cardImg = document.createElement('img');
            cardImg.setAttribute('src', img);
            const cardName = document.createElement('h5');
            cardName.innerHTML = name;
            const cardRace = document.createElement('div');
            cardRace.innerHTML = species;
            const cardLocation = document.createElement('div');
            cardLocation.innerHTML = location;
            const cardEpisode = document.createElement('div');
            cardEpisode.setAttribute('class', 'card__episode');
            cardEpisode.innerHTML = episodes;
            const cardDate = document.createElement('div');
            cardDate.setAttribute('class', 'card__date');
            cardDate.innerHTML = created;
            const cardCross = document.createElement('div');
            cardCross.setAttribute('class', 'card__cross');

            //append element

            const card = document.createElement('div');
            card.setAttribute('class', 'card');
            card.appendChild(cardCross);
            card.appendChild(cardImg);
            card.appendChild(cardName);
            card.appendChild(cardRace);
            card.appendChild(cardLocation);
            card.appendChild(cardEpisode);
            card.appendChild(cardDate);

            cardsBox.appendChild(card);




        });
    })
    .then(contentControl => {
        const cardsList = document.querySelectorAll('.card');
        const cardsBody = document.querySelector('.cards');
        const hideBtn = document.querySelector('.hide-btn');
        const filterDateMore = document.querySelector('.sort-filter__date-more');
        const filterDateLess = document.querySelector('.sort-filter__date-less');
        const cardDate = document.querySelectorAll('.card__date');
        const cardsCrossList = document.querySelectorAll('.card__cross');
        const cardsEpisodes = document.querySelectorAll('.card__episode');
        const filterEpisodeMore = document.querySelector('.sort-filter__episodes-more');
        const filterEpisodeLess = document.querySelector('.sort-filter__episodes-less');


        //Delete card
        for (let i = 0; i < cardsCrossList.length; i++) {
            cardsCrossList[i].addEventListener("click", function() {
                cardsCrossList[i].parentNode.remove();
            });
        };

        //Show cards
        for (let i = 0; i < 10; i++) {
            cardsList[i].classList.add('show');
        }

        window.addEventListener('scroll', function() {

            let windowRelativeBottom = document.documentElement.getBoundingClientRect().bottom;
            if (windowRelativeBottom == document.documentElement.clientHeight) {

                for (let i = 10; i < cardsList.length; i++) {
                    cardsList[i].classList.add('show');
                }

                hideBtn.classList.add('show');
            }
        });

        //Hide cards
        hideBtn.addEventListener("click", function(e) {
            e.preventDefault();
            for (let i = 10; i < cardsList.length; i++) {
                cardsList[i].classList.remove('show');
            }
            window.scrollTo(pageXOffset, 0);
            hideBtn.classList.remove('show');
        });

        /*Date filter**************************************************************************/

        let dateStr = '';
        for (let i = 0; i < cardDate.length; i++) {
            dateStr = cardDate[i].innerHTML;
            dateStr = dateStr.replace(/[^\d]/g, '');
            cardsList[i].setAttribute('data-date', dateStr);
        };

        /*Filter More*/
        filterDateMore.addEventListener('click', function(e) {
            e.preventDefault();

            function mySortAscending(a, b) {
                if (a.dataset.date < b.dataset.date)
                    return 1
                if (a.dataset.date > b.dataset.date)
                    return -1
                return 0
            };

            let sortedElms = Array.prototype.slice.call(cardsList);
            sortedElms.sort(mySortAscending);
            cardsBody.innerHTML = '';

            for (let i = 0; i < sortedElms.length; i++) {
                cardsBody.appendChild(sortedElms[i]);
            };

        });

        /*Filter Less*/
        filterDateLess.addEventListener('click', function(e) {
            e.preventDefault();

            function mySortDecline(a, b) {
                if (a.dataset.date > b.dataset.date)
                    return 1
                if (a.dataset.date < b.dataset.date)
                    return -1
                return 0
            };

            let sortedElms = Array.prototype.slice.call(cardsList);
            sortedElms.sort(mySortDecline);
            cardsBody.innerHTML = '';

            for (let i = 0; i < sortedElms.length; i++) {
                cardsBody.appendChild(sortedElms[i]);
            };

        });

        /***************************************************************************************************/

        /*Episodes filter*********************************************************************************/
        let episodeStr;
        for (let i = 0; i < cardsEpisodes.length; i++) {
            episodeStr = cardsEpisodes[i].querySelectorAll('span').length;
            console.log(episodeStr);
            cardsList[i].setAttribute('data-episodes', episodeStr);
        };

        /*Filter Less*/
        filterEpisodeLess.addEventListener('click', function(e) {
            e.preventDefault();

            function mySortDecline(a, b) {
                if (a.dataset.episodes == b.dataset.episodes) {
                    if (a.dataset.date > b.dataset.date)
                        return 1
                    if (a.dataset.date < b.dataset.date)
                        return -1
                    return 0
                }
                if (a.dataset.episodes > b.dataset.episodes)
                    return 1
                if (a.dataset.episodes < b.dataset.episodes)
                    return -1
                return 0
            };

            let sortedElms = Array.prototype.slice.call(cardsList);
            sortedElms.sort(mySortDecline);
            cardsBody.innerHTML = '';

            for (let i = 0; i < sortedElms.length; i++) {
                cardsBody.appendChild(sortedElms[i]);
            };

        });

        /*Filter More*/
        filterEpisodeMore.addEventListener('click', function(e) {
            e.preventDefault();

            function mySortAscending(a, b) {
                if (a.dataset.episodes == b.dataset.episodes) {
                    if (a.dataset.date < b.dataset.date)
                        return 1
                    if (a.dataset.date > b.dataset.date)
                        return -1
                    return 0
                }
                if (a.dataset.episodes < b.dataset.episodes)
                    return 1
                if (a.dataset.episodes > b.dataset.episodes)
                    return -1
                return 0
            };

            let sortedElms = Array.prototype.slice.call(cardsList);
            sortedElms.sort(mySortAscending);
            cardsBody.innerHTML = '';

            for (let i = 0; i < sortedElms.length; i++) {
                cardsBody.appendChild(sortedElms[i]);
            };

        });

        /**************************************************************************************************/
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });